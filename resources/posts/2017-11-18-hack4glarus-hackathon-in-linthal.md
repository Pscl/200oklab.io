---
title: Hack4Glarus Hackathon in Linthal
authors: Alain M. Lafon
category: 200ok
date-created: 2017-11-18
tags: 200ok, ungleich, ungleich glarus ag, 
uuid: 65b2ebe34ef2b65bdcbfee845a109811
---

Hack4Glarus(https://hack4glarus.ch/) is the first hackathon of Glarus. It is your chance to meet & hack with geniuses of Switzerland. It is all about technology and fun. 

This event is hosted at a great location
([same as the launch](http://200ok.ch/posts/joint-release-of-swiss-crowdfunder.html)
of [swiss-crowdfunder.com](https://swiss-crowdfunder.com) and is
hosted by our friends from [ungleich glarus ag](https://ungleich.ch/).

We from 200ok will be there as well - as are our friends from the
[Insopor Zen Academy](http://zen-temple.net/). Join us for three great
days of hacking, fun and learning!

Join the event now on the [dedicated page](https://hack4glarus.ch/),
on [ Facebook ](https://www.facebook.com/events/1738729979502962/) or
on [ Meetup ](https://www.meetup.com/Digital-Glarus-Business-Technology/events/244179551/).

![](/img/2017-11-18/hack4glarus.jpg)

## More information

**What will be the hack topics?**

Anything that is related to Glarus, that can improve lives of people. Below are some fun hack examples. Of course the topics will not be limited to the list below, you can come up with your own project!

- tracking cows & sheep
- automate farming
- easier payments for people
- introduce IoT / LoRaWAN
- automate stuff
- how to keep servers fresh 
- temperature monitoring
- monitor servers 
- measure water usage
- measure electricity
- measure traffic and put online
- drones


**Where does Hack4Glarus take place?**

It will happen at Spinnerei Linthal, a very cool old factory hall at Linthal. It's also where the new data center from ungleich, Data Center Light is.

What will be provided at Hack4Glarus?

- food and drinks
- place to sleep
- very cool environment

What do I need to bring to Hack4Glarus?

- great ideas
- a sleeping bag 
- your awesome self

And there’s an extra: Fridolinpass.

If you have an extraordinarily good idea, apply for Fridolinpass! Write us why you should participate Hack4Glarus, and how will the community benefit from your hacking. For the ones who win Fridonlinpass we will cover her/his travel cost within Switzerland to Hack4Glarus. 

**How can I apply?**

Apply here and submit your ideas: https://docs.google.com/forms/d/e/1FAIpQLSezSIJwO5gcvwrAhxHjsqwXM72eiU7j627olhgWKGDHHIaoWQ/viewform?usp=sf_link

**Attention: only limited number of seats are available. Apply now!**
