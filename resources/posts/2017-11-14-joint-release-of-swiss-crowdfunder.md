---
title: Joint release of swiss-crowdfunder.com with ungleich glarus ag
authors: Alain M. Lafon
category: 200ok
date-created: 2017-11-14
tags: 200ok, crowdfunding, swiss crowdfunder, products, release, ungleich, ungleich glarus ag
uuid: 0fe5d5cf-0622-4abc-825a-9c8c0f8ba12d
---

We just released our newest product: [Swiss Crowdfunder](https://swiss-crowdfunder.com)

*Swiss Crowdfunder* is a joint product that we built together with our
good friends from [ungleich glarus ag](https://ungleich.ch/). As the
name implies it is a crowdfunding platform, but with a twist! Whereas
traditional crowdfunding campaign supporters will only receive certain
goodies (like stickers, t-shirts or even newly created products),
supporters on *Swiss Crowdfunder* can actually invest in companies.

Equity funding (or crowdinvestment) enables the supporters to
effectively buy shares from an otherwise not publicly traded company.
Therefore it is a great option to raise money for companies on the one
hand, but on the other hand it is an even greater option for the
general public to invest into startups and vested companies alike.

There's already a juicy
[campaign online](https://swiss-crowdfunding.net/campaigns/ungleich-glarus-ag),
too! ungleich glarus ag is expanding their
[Data Center Light](https://www.datacenterlight.ch/) to another
location. The new location is in Linthal, GL (Switzerland) and is
perfectly situated within great infrastructure - the new location even
has its own water power plant, so the whole data center is going 100%
green! Apart from that, they are very serious about open source -
their whole tech stack
[ runs on open source software ](https://datacenterlight.ch/en-us/datacenterlight/whydatacenterlight).

Their plans work out well so far. To accelerate their expansion plans,
they are raising CHF250,000 and are giving everyone the option to buy
shares.

On Saturday, we had a great launch event at the new location. I'll
leave you with some impressions.

**Countdown to launch**
![](/img/2017-11-14/dcl-crowdfunding-1.jpg)

**Alain enjoys the launch event, but keeps on coding!**
![](/img/2017-11-14/dcl-crowdfunding-2.jpg)

**Live music**

[Rob Moir](http://www.robmoir.com/) made a fantastic concert -
unplugged, just him and his guitar. It was amazing!
![](/img/2017-11-14/dcl-crowdfunding-3.jpg)

**Friendly get together of like-minded people**
![](/img/2017-11-14/dcl-crowdfunding-4.jpg)
