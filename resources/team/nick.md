---
name: Nick Niles
title: VP of Design
email: nick@200ok.ch
image: /img/nick.jpg
position: 30
uuid: 50403e2f-5da2-4e11-98ad-686b25889432
---
A former rock musician and long-distance runner from Northern
California, Nick has been designing and building web sites
since 1997. With a background in design & technology, and an MFA in
Film Directing from Columbia University, he enjoys telling stories
through motion graphics, animations, and rich digital experiences.