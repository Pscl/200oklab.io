---
name: Ursula Ledergerber
title: Head of Communications
email: ursula@200ok.ch
image: /img/ursula.jpg
position: 50
uuid: f361e54b-5698-442c-a339-f1356efbb531
---
Sales, Public Relations, Marketing.
