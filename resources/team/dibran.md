---
name: Dibran Isufi
title: Code Gluer
email: dibran@200ok.ch
image: /img/dibran.jpg
position: 40
uuid: 28f71257-ead7-4599-b6fd-454b6abc01f3
---
Glues all the code together.
