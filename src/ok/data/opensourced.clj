(ns ok.data.opensourced
  (:require [ok.layout :as layout]))

(defn- opensource
  [opensource]
  [:li 
    [:div.info
  [:h3 (:name opensource)]
  [:p.description (:description opensource)]]])

(defn page
  [arg]
  (let [db (get-in arg [:meta :fsdb :manifest])
        opensourced (->> db :projects vals (filter :opensourced))]
    (layout/main arg
                  [:main.opensourced
                  [:h1 "Projects We&rsquo;ve Open-Sourced"]
                  [:ul.opensourced-list
                   (map opensource opensourced)]])))
