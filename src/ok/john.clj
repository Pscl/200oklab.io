(ns ok.john)

(defn mccarthy []
  "<!--
MMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMNNNNMMNmmmmNNNNNNNNNNmmmNMNNmmNNMMNmmddmNNNMMNmddmNNMMNNddmNMMMmdhdd
MMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMNNNNMMNmmmmNNNNNNNNNNmmmNMMNmmNNMMNNdddmmNNMMNmddmNNMMMNmdmNMMMmdhdd
MMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMNNNNMMNmmmmNNNNNmmhmNddhmNMNmmmNMMMNmddmmNNMMNmddmmNMMMNmdmNMMMNdddd
MMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMNNNNMMNNmmmNNmdhs+oosso/-:/oydmNMMMNmddmmNNMMMmmdmmNMMMNmdmNMMMNdhdd
MMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMNNNNMMNmdhs+/-..`````.::/-```-oNNNMNmddmmNNMMMNddmmNMMMNmdmNNMMNmddd
MMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMNNNNMNdo:.```````````````-:```./mNNNmdddmmNNMMNmddmNMMMNmdmNNMMNmddd
MMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMNNmdo-............```` ````````-hddmdddmmNNMMNmddmNMMMNmddmNMMNmddd
MMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMmyo/------://:-.`...````.````````-/ossydmmNNMMNmddmNNMMNmddmNMMNmddd
MMMMMMMMMMMMMMMMMMMMMMMMMMMMNMNs:---::///:-:--.````.`...````.`.`.``--:+yhmNNMMNmddmmNMMMNmdmNMMMmddd
MMMMMMMMMMMMMMMMMMMMMMMMMMMMNy/.-::////--:-:/:......-.-....``.----....:oydNNMMMmddmmNMMMNmdmNMMMmdhd
MMMMMMMMMMMMMMMMMMMMMMMMMMNy+--...://+o:./oyyo+///+/+/://:--...-:::-..`./sdNMMMNmddmNMMMNmmmNMMMNddd
MMMMMMMMMMMMMMMMMMMMMMMMMNs:---...-://+/:ossyo+ssyss///:---.....-::::-.``.ohmMMNmddmNMMMNmdmNMMMNmdd
MMMMMMMMMMMMMMMMMMMMMMMMNs/::-....--:////::/-/-osooo///::--..--:+s:::--.``-ohNNNmddmNMMMNmdmNMMMNmdd
MMMMMMMMMMMMMMMMMMMMMMMNy///:--..`..-::--......://+++++++/:---::/../oo/--`-:yNMNmddmmNMMNmdmNNMMNmdd
MMMMMMMMMMMMMMMMMMMMMMMmyso+/:.````.....````````.---/+sso++++o/:--+syhh+:..omNMNmdddmNMMNmmmmNMMNmdd
MMMMMMMMMMMMMMMMMMMMMMNhsoo+/-..::-.......````   ```.:+sysssyyyyyo/:ohhy+:-/hNMNmdddmNMMNNmmmNMMNmdd
MMMMMMMMMMMMMMMMMMMMMMNdysoo//+sys/-.-:--..`````   ```.:++/+oyhdmho+shdhs/-./dMMNmddmNMMMNmmmNMMNmmd
MMMMMMMMMMMMMMMMMMMMMMMNdyysosyhyo/---:::--...`````````.-/+++osyhdmmhddhs+:-/dMMNmddmNMMMNmmmNMMMNmd
MMMMMMMMMMMMMMMMMMMMMMMMmhhysyyyo/:::-://::-..``````````..-:://oyhdNmddhs/:-/hNMNmddmNMMMNmmmNMMMNmd
MMMMMMMMMMMMMMMMMMMMMMMMNdhhhhhyso++++////:--.``` ```````````.-:oydmmmho++/:+hNMNmmdmNMMMNmmmNMMMNmd
MMMMMMMMMMMMMMMMMMMMMMMMNdddhddhyyyhyso//:/----..````````   ```-/ohdmds/:+//hmNMNmmdmmNMMNmmmNMMMNmd
MMMMMMMMMMMMMMMMMMMMMMMMMNmmddmmmmdysoo//+syyhhyo+//:-.........-:+shhho//+++hmNMMNmmmmNMMNNmmNMMMNmm
MMMMMMMMMMMMMMMMMMMMMMMMMMmyydNMMMNNmmmddmddddmmddddys::/+++::////oydhsoosshmmNMMNmmmmNMMNNmmNMMMNmm
MMMMMMMMMMMMMMMMMMMMMMMMMNmNmddmmmhhhmMMNdhhhdmNNNNNmhooyhyyyyhhhhhdmddyyhdmmNMMMNmmmmNMMMNmmNNMMNmm
MMMMMMMMMMMMMMMMMMMMMMMMMNNNmmhyhdys++mNmNNmNNmNNNNNNMNmNMNNNNNmmmmhmNMMNdmmmNMMMNmmmmNMMMNmmNNMMNmm
MMMMMMMMMMMMMMMMMMMMMMMMMMNmmdyshdds+:smsyyhdmmmmhhmNN++NMNmNNmmNmhyddNMmdmmmNNMMNmmmmNMMMNmmNNMMNNm
MMMMMMMMMMMMMMMMMMMMMMMMMMNddsyssddys+/NooosyhysoosdN+`.dNyhdmdddhsyymMMmdmmmNNMMMNmmmNNMMNmmNNMMNNm
MMMMMMMMMMMMMMMMMMMMMMMMMMMhshhyohddhhyydsoo+:::/smd:`` -my/oyyyy+osyNNNmdmmmNNMMMNmmmNNMMNmmmNMMMNm
MMMMMMMMMMMMMMMMMMMMMMMMMMMdsyddsydmdhhhyso+++osdmo--.```/dy+/++oossdmmmmmdmmNNMMMNmmmmNMMNmmmNMMMNm
MMMMMMMMMMMMMMMMMMMMMMMMMMMNhshdhsdmdhhysooooshmNy++sssoo++yo++oyhhsyhmmmdmmmmNMMMNmmmmNMMNNmmNMMMNm
MMMMMMMMMMMMMMMMMMMMMMMMMMMMMNmdhshdmdhhssssyhhhdmmNNNNNNmhdhsooshyoyNNNmmmmmmNNMMNmmmmNMMNNmmNMMMNm
MMMMMMMMMMMMMMMMMMMMMMMMMMMMMMmmdhhmmmhhyyyysydydsdhmNNmmyosyyhyhhyhNMNNmmddmmNNMMNmmmmNMMNNmmNNMMNm
MMMMMMMMMMMMMMMMMMMMMMMMMMMMMNmmmmdmNmhssyhyhdmmNmNmmNmmhyhs/shdhhhmMMNNNNNNNNNNMMMNNNNMMMMMMMMMMMMM
MMMMMMMMMMMMMMMMMMMMMMMMMMMNdmhmmmmmmdhosyydNNNNNmmmmmmmNNNdy/ydhsdNMMNMMMMMMMMMMMMMMMMMMMMMMMMMMMMM
MMMMMMMMMMMMMMMMMMMMMMMMMNdhmyydmmmmmhyydddmdhyhyhyhhhyyhmNNdoososmNNNNMMMMMMMMMMMMMMMMNNNNNmmdddhhh
MMMMMMMMMMMMMMMMMMMMMMMNdyyhhssydmmmmdddmmy++osoosoysoossydmdso+sdmNMNMMMMMMMNNNmmddhhyyyssss+++////
MMMMMMMMMMMMMMMMMMMMMmyooyydsoooyddmNdmmdo+://osossoyssysososyyoossyyyyydmNmddhhhhyyyysssssoo+////::
MMMMMMMMMMMMMMMMMMNho+o++yshoooooshmmmmhho:--:/o/shhhddyos+::ososo+//::----::://+osyssssssoooo+///::
MMMMMMMMMMMMMMNmyo///++++ssyyooossyhmmNhh+:::++/oyhhhhys+o+-+ssssooo++/::-------..-/sssssssoooo+////
MMMMMMMMMMMNds+:::///++++osshosssyyydmdho/::/+oosysyhhso/+/:sssssooooo++/::-------..-/osssssoooo+///
MMMMMMMMNho:---:::/::+++++oshsosyyyhhddyo+-://:/o/oyydo++/::hmyssooo+++++//::-----....:osssosooooo//
MMMMNho/:------::::://+oo+ooydossyyyhddmdhyyyo:/+/oyhhs++/:/hmdsoooo++///////:------...-osssssosoo++
Nds/-----------:::::///++ooosdhosyyhhhddmNmmNmmmhdhddhsoo+/+yMNhs++++++/////:::-----.-.--sssssssoooo
--......------::::::///++++osymhoyyhhhhdmmmmNNNNNMMMMNhso++++hMmyo++++++////:::::-----.--/ssssssoooo
.........---::-:/::://///ooooshdhsyhhhddmmmmmmmMMMMMMMMNs+++/ohMds++/+/++///:::::::----.--:osssssooo
........-s+----::--:/::/++++ooyhmhyhddhddmmmmdMMMMMMMMMMMs///+sNNho+////////+++//::::--.---:sssssooo
.........-oh/--::--::::/+++oo+oysdhyhhdddmmmdNMMMMMMMMMMMMs/:/smMmy+//////:::::::////:-----/syssssso
-->
")
